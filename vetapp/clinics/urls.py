from django.urls import path, re_path
from . import views

urlpatterns = [
    path(
        '',
        views.index,
        name='index'
    ),
    re_path(
        r'^vet/$',
        views.index_vet,
        name='index_vet'
    ),
    re_path(
        r'^appointment/(?P<pk>\d+)/set_potential_dates/$',
        views.set_potential_dates,
        name='set_potential_dates',
    ),
    re_path(
        r'^appointment/all/$',
        views.all_appointments,
        name='all_appointments',
    ),
    re_path(
        r'^appointment/rejected/$',
        views.rejected_appointments,
        name='rejected_appointments',
    ),
    re_path(
        r'^appointment/emergency/$',
        views.add_emergency_appointment,
        name='add_emergency_appointment',
    ),
]
