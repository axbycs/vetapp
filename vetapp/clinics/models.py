from django.conf import settings
from django.db import models

class Veterinarian(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    DNI = models.CharField(
        max_length=9,
    )
    phone_number = models.CharField(
        max_length=9,
    )
    country = models.CharField(
        max_length=100,
    )
    town = models.CharField(
        max_length=100,
    )
    postal_code = models.CharField(
        max_length=5,
    )
    street = models.CharField(
        max_length=100,
    )
    local_number = models.CharField(
        max_length=10,
        blank=True,
    )
    specializations = models.TextField(
        blank = True,
    )
    created = models.DateTimeField(
        auto_now_add=True,
    )
    updated = models.DateTimeField(
        auto_now=True,
    )

    def __str__(self):
        return self.user.username

class Clinic(models.Model):
    in_charge = models.OneToOneField(
        Veterinarian,
        on_delete=models.CASCADE
    )
    email = models.CharField(
        max_length=200,
    )
    phone_number = models.CharField(
        max_length=9,
    )
    country = models.CharField(
        max_length=100,
    )
    town = models.CharField(
        max_length=100,
    )
    postal_code = models.CharField(
        max_length=5,
    )
    street = models.CharField(
        max_length=100,
    )
    local_number = models.CharField(
        max_length=10,
        blank=True,
    )
    basic_information = models.TextField(
        blank = True,
    )
    created = models.DateTimeField(
        auto_now_add=True,
    )
    updated = models.DateTimeField(
        auto_now=True,
    )

    def __str__(self):
        return (self.email + 
        " - " + self.town +
        " " + self.street +
        " " + self.local_number)
