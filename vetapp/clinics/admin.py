from django.contrib import admin
from .models import Clinic, Veterinarian

class VeterinarianAdmin(admin.ModelAdmin):
    model = Veterinarian
    list_display = ('DNI', 'user', 'get_first_name', 'get_last_name', 'created', 'updated',)
    list_filter = ('created', 'updated',)

    def get_first_name(self, obj):
        return obj.user.first_name
    get_first_name.short_description = 'First name'

    def get_last_name(self, obj):
        return obj.user.last_name
    get_last_name.short_description = 'Last name'

admin.site.register(Veterinarian, VeterinarianAdmin)

class ClinicAdmin(admin.ModelAdmin):
    model = Clinic
    list_display = ('email', 'in_charge', 'phone_number', 'street', 'local_number', 'created', 'updated',)
    list_filter = ('created', 'updated',)

admin.site.register(Clinic, ClinicAdmin)