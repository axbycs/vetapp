from datetime import datetime

from django.shortcuts import render, redirect
from django_filters.views import FilterView
from django.views.generic.edit import UpdateView, FormView
from django.shortcuts import redirect

from appointments.models import Appointment
from .filters import RequestedAppointmentFilter, AllAppointmentFilter
from .forms import SetPotentialDatesForm, EmergencyAppointmentForm

def index(request):
    if request.user.is_authenticated:
        if request.user.groups.filter(name='vet'):
            return redirect('vet/')
        if request.user.groups.filter(name='clients'):
            return redirect('client/')
        return render(request, 'index.html')
    else:
        return redirect('accounts/login/')

class IndexVetView(FilterView):
    model = Appointment
    template_name = "index_vet.html"
    filterset_class = RequestedAppointmentFilter

    def get_queryset(self):
        return Appointment.objects.filter(status='requested')

index_vet = IndexVetView.as_view()

class SetPotentialDatesView(UpdateView):
    model = Appointment
    template_name = "vet/set_potential_dates.html"
    form_class = SetPotentialDatesForm

    def get_object(self):
        return Appointment.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        appointment = form.save(commit=False)
        appointment.status = 'potential'
        appointment.save()
        return redirect('/')

set_potential_dates = SetPotentialDatesView.as_view()

class AllAppointmentsView(FilterView):
    model = Appointment
    template_name = "appointments/all_appointments.html"
    filterset_class = AllAppointmentFilter

    def get_queryset(self):
        return Appointment.objects.all()

all_appointments = AllAppointmentsView.as_view()

class RejectedAppointmentView(FilterView):
    model = Appointment
    template_name = "appointments/rejected_appointments.html"
    filterset_class = RequestedAppointmentFilter

    def get_queryset(self):
        return Appointment.objects.filter(status='rejected')

rejected_appointments = RejectedAppointmentView.as_view()

class AddEmergencyAppointmentView(FormView):
    model = Appointment
    template_name = "appointments/add_emergency_appointment.html"
    form_class = EmergencyAppointmentForm

    def form_valid(self, form):
        appointment = form.save(commit=False)
        appointment.status = 'accepted'
        appointment.date_time = datetime.now()
        appointment.save()
        return redirect('/')
            
add_emergency_appointment = AddEmergencyAppointmentView.as_view()

