from django.forms import ModelForm
from django import forms
from bootstrap_datepicker_plus import DateTimePickerInput

from appointments.models import Appointment

class SetPotentialDatesForm(ModelForm):

    potential1 = forms.DateTimeField(
        widget=DateTimePickerInput()
    )

    potential2 = forms.DateTimeField(
        widget=DateTimePickerInput()
    )

    potential3 = forms.DateTimeField(
        widget=DateTimePickerInput()
    )

    class Meta:
        model = Appointment
        fields = ['potential1', 'potential2', 'potential3']

class EmergencyAppointmentForm(ModelForm):

    class Meta:
        model = Appointment
        fields = ['attendant', 'clinic', 'pet']