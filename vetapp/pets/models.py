from django.db import models
from clients.models import Client

class Species(models.Model):
    species = models.CharField(
        max_length=100,
    )
    race = models.CharField(
        max_length=100,
    )
    aspects = models.TextField(
        blank = True,
    )
    created = models.DateTimeField(
        auto_now_add=True,
    )
    updated = models.DateTimeField(
        auto_now=True,
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['species', 'race'], name='unique_species'),
        ]

    def __str__(self):
        return self.species + " - " + self.race

class Pet(models.Model):
    owner = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
    )
    species = models.ForeignKey(
        Species,
        on_delete=models.CASCADE,
    )
    pet_name = models.CharField(
        max_length=50,
    )
    xip = models.CharField(
        max_length=15,
    )
    born_date = models.DateField(
    )
    death_date = models.DateField(
        null=True,
        blank=True,
    )
    weight = models.IntegerField(
    )
    height = models.IntegerField(
    )
    photo = models.ImageField(
        upload_to='pets',
    )
    comments = models.TextField(
        blank = True,
    )
    created = models.DateTimeField(
        auto_now_add=True,
    )
    updated = models.DateTimeField(
        auto_now=True,
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['xip',], name='unique_xip'),
        ]

    def __str__(self):
        return self.owner.user.username + " - " + self.pet_name
