from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django_filters.views import FilterView 
from django.shortcuts import redirect

from .models import Pet
from .forms import NewPetForm

class YourPetsView(ListView):
    model = Pet
    template_name = "pets/your_pets.html"

    def get_queryset(self):
        return Pet.objects.filter(owner__user__username=self.request.user.username)

your_pets = YourPetsView.as_view()

class AddNewPetView(FormView):
    model = Pet
    template_name = "pets/add_new_pet.html"
    form_class = NewPetForm
    #success_url = 'pets/'

    #def form_valid(self, form):
    #    pet = form.save(commit=False)
    #    pet.save()
    #    return redirect('pets/')
            
add_new_pet = AddNewPetView.as_view()

class AllPetsView(FilterView):
    model = Pet
    template_name = "pets/all_pets.html"

    def get_queryset(self):
        return Pet.objects.filter()

all_pets = AllPetsView.as_view()
