from django.forms import ModelForm
from django import forms

from .models import Pet

class NewPetForm(ModelForm):

    class Meta:
        model = Pet
        fields = ['pet_name', 'xip', 'born_date', 'weight', 'height',]
