from django.contrib import admin
from .models import Pet, Species

class SpeciesAdmin(admin.ModelAdmin):
    model = Species
    list_display = ('species', 'race', 'created', 'updated',)
    list_filter = ('created', 'updated',)

admin.site.register(Species, SpeciesAdmin)

class PetAdmin(admin.ModelAdmin):
    model = Pet
    list_display = ('pet_name','owner', 'species', 'xip', 'created', 'updated')
    list_filter = ('created', 'updated',)

admin.site.register(Pet, PetAdmin)