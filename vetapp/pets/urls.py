from django.urls import path, re_path
from . import views

urlpatterns = [
    re_path(
        r'^pets/$',
        views.your_pets,
        name='your_pets',
    ),
    re_path(
        r'^pets/add_pet/$',
        views.add_new_pet,
        name='add_new_pet',
    ),
    re_path(
        r'^pets/all/$',
        views.all_pets,
        name='all_pets',
    ),
]