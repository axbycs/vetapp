from django.urls import path, re_path
from . import views

urlpatterns = [
    re_path(
        r'^apointments/next/$',
        views.your_next_appointments,
        name='your_next_appointments'
    ),
    re_path(
        r'^apointments/potencial/$',
        views.your_potencial_appointments,
        name='your_potencial_appointments'
    ),
    re_path(
        r'^apointments/past/$',
        views.your_past_appointments,
        name='your_past_appointments'
    ),
    re_path(
        r'^apointments/new/$',
        views.new_appointment,
        name='add_new_appointment'
    ),
    re_path(
        r'^apointments/(?P<pk>\d+)/cancel/$',
        views.cancel_appointment,
        name='cancel_appointment',
    ),
    re_path(
        r'^apointments/(?P<pk>\d+)/reject/$',
        views.reject_appointment,
        name='reject_appointment',
    ),
    re_path(
        r'^apointments/(?P<pk>\d+)/accept_date/(?P<potential>\d+)/$',
        views.accept_date,
        name='accept_date',
    ),
]
