from django.conf import settings
from django.db import models
from clinics.models import Clinic, Veterinarian
from pets.models import Pet
from django.utils.translation import gettext_lazy as _

STATUS_CHOICES=[
    ('requested',_('Requested'),),
    ('potential',_('Potential'),),
    ('rejected',_('Rejected'),),
    ('accepted',_('Accepted'),),
    ('cancelled',_('Cancelled'),),
]

class Appointment(models.Model):
    attendant =  models.ForeignKey(
        Veterinarian,
        on_delete=models.CASCADE,
    )
    clinic = models.ForeignKey(
        Clinic,
        on_delete=models.CASCADE,
    )
    pet = models.ForeignKey(
        Pet,
        on_delete=models.CASCADE,
    )
    date_time = models.DateTimeField(
        blank=True,
        null=True,
    )
    potential1 = models.DateTimeField(
        blank=True,
        null=True,
    )
    potential2 = models.DateTimeField(
        blank=True,
        null=True,
    )
    potential3 = models.DateTimeField(
        blank=True,
        null=True,
    )
    status = models.CharField(
        max_length=9,
        choices=STATUS_CHOICES,
        default='requested',
    )
    comments = models.TextField(
        blank = True,
    )
    created = models.DateTimeField(
        auto_now_add=True,
    )
    updated = models.DateTimeField(
        auto_now=True,
    )

    def __str__(self):
        return (self.attendant.user.username +
            " - " + self.clinic.email +
            " - " + self.pet.pet_name +
            " - " + self.pet.owner.user.username)
    