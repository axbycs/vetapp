from django.forms import ModelForm
from django import forms

from .models import Appointment
from pets.models import Pet

class NewAppointmentForm(ModelForm):
    pet = forms.ModelChoiceField(queryset=Pet.objects.all())

    class Meta:
        model = Appointment
        fields = ['attendant', 'clinic', 'pet', 'comments',]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        self.fields['pet'].queryset = Pet.objects.filter(owner__user__username=user.username)


class CancelAppointmentForm(ModelForm):

    class Meta:
        model = Appointment
        fields = []

class RejectAppointmentForm(ModelForm):

    class Meta:
        model = Appointment
        fields = []