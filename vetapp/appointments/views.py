from datetime import datetime

from django_filters.views import FilterView 
from django.views.generic.edit import FormView
from django.shortcuts import redirect

from .models import Appointment
from clients.models import Client
from pets.models import Pet
from .filters import ClientAppointmentFilter, PotentialAppointmentFilter
from .forms import NewAppointmentForm, CancelAppointmentForm, RejectAppointmentForm

class YourNextAppointmentView(FilterView):
    model = Appointment
    template_name = "appointments/your_next_appointments.html"
    filterset_class = ClientAppointmentFilter

    def get_queryset(self):
        return Appointment.objects.filter(pet__owner__user__username=self.request.user.username,
            date_time__gt=datetime.now(), status='accepted')

your_next_appointments = YourNextAppointmentView.as_view()

class YourPotencialAppointmentView(FilterView):
    model = Appointment
    template_name = "appointments/your_potencial_appointments.html"
    filterset_class = PotentialAppointmentFilter

    def get_queryset(self):
        return Appointment.objects.filter(pet__owner__user__username=self.request.user.username,
            status__in=('requested','potential','rejected',))

your_potencial_appointments = YourPotencialAppointmentView.as_view()

class YourPastAppointmentView(FilterView):
    model = Appointment
    template_name = "appointments/your_past_appointments.html"
    filterset_class = ClientAppointmentFilter

    def get_queryset(self):
        return Appointment.objects.filter(pet__owner__user__username=self.request.user.username,
            date_time__lt=datetime.now())

your_past_appointments = YourPastAppointmentView.as_view()

class NewAppointmentView(FormView):
    model = Appointment
    template_name = "appointments/add_new_appointment.html"
    form_class = NewAppointmentForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form):
        appointment = form.save(commit=False)
        appointment.status = 'requested'
        appointment.save()
        return redirect('/')
            
new_appointment = NewAppointmentView.as_view()


class CancelAppointmentView(FormView):
    model = Appointment
    template_name = "appointments/cancel_appointment.html"
    form_class = CancelAppointmentForm

    @property
    def get_appointment(self):
        return Appointment.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        appointment = self.get_appointment
        appointment.status = 'cancelled'
        appointment.save()
        return redirect('/')
    
cancel_appointment = CancelAppointmentView.as_view()

class RejectAppointmentView(FormView):
    model = Appointment
    template_name = "appointments/reject_appointment.html"
    form_class = RejectAppointmentForm

    @property
    def get_appointment(self):
        return Appointment.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        appointment = self.get_appointment
        appointment.status = 'rejected'
        appointment.save()
        return redirect('/')
    
reject_appointment = RejectAppointmentView.as_view()

class AcceptDateView(FormView):
    model = Appointment
    template_name = "appointments/accept_date.html"
    form_class = RejectAppointmentForm

    @property
    def get_appointment(self):
        return Appointment.objects.get(pk=self.kwargs['pk'])

    @property
    def get_potential(self):
        potential = self.kwargs['potential']
        print(potential)
        if potential == '1':
            return self.get_appointment.potential1
        if potential == 2:
            return self.get_appointment.potential2
        if potential == 3:
            return self.get_appointment.potential2

    def form_valid(self, form):
        appointment = self.get_appointment
        appointment.date = self.get_potential
        appointment.status = 'accepted'
        appointment.save()
        return redirect('/')
    
accept_date = AcceptDateView.as_view()