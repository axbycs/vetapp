from datetime import datetime

import django_filters
from django.conf import settings
from django import forms
from bootstrap_datepicker_plus import DateTimePickerInput
from django.utils.translation import gettext_lazy as _

from .models import Appointment

STATUS_CHOICES=[
    ('requested',_('Requested'),),
    ('potential',_('Potential'),),
    ('rejected',_('Rejected'),),
]

class ClientAppointmentFilter(django_filters.FilterSet):
    attendant = django_filters.CharFilter(method='attendant_filter')
    pet = django_filters.CharFilter(method='pet_filter')
    date_time_gte = django_filters.DateTimeFilter(
        label='From date',
        method='date_time_gte_filter',
        widget=DateTimePickerInput(
            #format=settings.DATETIME_INPUT_FORMAT
        )
    )
    date_time_lte = django_filters.DateTimeFilter(
        label='Until date',
        method='date_time_lte_filter',
        widget=DateTimePickerInput(
            #format=settings.DATETIME_INPUT_FORMAT
        )
    )
    
    def attendant_filter(self, queryset, name, value):
        return queryset.filter(attendant__user__username__icontains=value)
    
    def pet_filter(self, queryset, name, value):
        return queryset.filter(pet__pet_name__icontains=value)

    def date_time_gte_filter(self, queryset, name, value):
        print(value)
        return queryset.filter(date_time__gte=value)

    def date_time_lte_filter(self, queryset, name, value):
        print(value)
        return queryset.filter(date_time__lte=value)

    class Meta:
        model = Appointment
        fields = ['attendant', 'pet', 'date_time_gte', 'date_time_lte',]

class PotentialAppointmentFilter(django_filters.FilterSet):
    attendant = django_filters.CharFilter(method='attendant_filter')
    pet = django_filters.CharFilter(method='pet_filter')
    status = django_filters.ChoiceFilter(choices=STATUS_CHOICES)
    
    def attendant_filter(self, queryset, name, value):
        return queryset.filter(attendant__user__username__icontains=value)
    
    def pet_filter(self, queryset, name, value):
        return queryset.filter(pet__pet_name__icontains=value)

    class Meta:
        model = Appointment
        fields = ['attendant', 'pet', 'status']