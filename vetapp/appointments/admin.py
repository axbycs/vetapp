from django.contrib import admin
from .models import Appointment

class AppointmentAdmin(admin.ModelAdmin):
    model = Appointment
    list_display = ('attendant', 'clinic', 'pet', 'date_time', 'status', 'created', 'updated')
    list_filter = ('date_time', 'created', 'updated',)

admin.site.register(Appointment, AppointmentAdmin)