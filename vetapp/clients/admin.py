from django.contrib import admin
from .models import Client

class ClientAdmin(admin.ModelAdmin):
    model = Client
    list_display = ('user', 'get_first_name', 'get_last_name', 'created', 'updated',)
    list_filter = ('created', 'updated',)

    def get_first_name(self, obj):
        return obj.user.first_name
    get_first_name.short_description = 'First name'

    def get_last_name(self, obj):
        return obj.user.last_name
    get_last_name.short_description = 'Last name'

admin.site.register(Client, ClientAdmin)