from datetime import datetime

import django_filters
from django.conf import settings

from .models import Client

class ClientFilter(django_filters.FilterSet):
    username = django_filters.CharFilter(method='username_filter')
    first_name = django_filters.CharFilter(method='first_name_filter')
    last_name = django_filters.CharFilter(method='last_name_filter') 

    def username_filter(self, queryset, name, value):
        return queryset.filter(user__username=value)

    def first_name_filter(self, queryset, name, value):
        return queryset.filter(user__first_name=value)

    def last_name_filter(self, queryset, name, value):
        return queryset.filter(user__last_name=value)

    class Meta:
        model = Client
        fields = ['user', 'first_name', 'last_name',]