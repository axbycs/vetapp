from django.urls import path, re_path
from . import views

urlpatterns = [
    re_path(
        r'^client/$',
        views.index_client,
        name='index_client',
    ),
    re_path(
        r'^client/all/$',
        views.all_clients,
        name='all_clients',
    )
]
