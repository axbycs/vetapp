from datetime import datetime

from django_filters.views import FilterView 
from django.views.generic.detail import DetailView

from appointments.models import Appointment
from .models import Client
from .filters import ClientFilter

class IndexClientView(DetailView):
    model = Appointment
    template_name = "index_client.html"

    def get_object(self):
        return Appointment.objects.filter(pet__owner__user__username=self.request.user.username,
            date_time__gt=datetime.now(), status='accepted').order_by('date_time').first()

index_client = IndexClientView.as_view()


class AllClientsView(FilterView):
    model = Client
    template_name = "clients/all_clients.html"
    filter_class = ClientFilter

    def get_queryset(self):
        return Client.objects.all()

all_clients = AllClientsView.as_view()
