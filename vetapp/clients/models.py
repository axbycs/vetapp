from django.conf import settings
from django.db import models

class Client(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    DNI = models.CharField(
        max_length=9,
        null=True,
    )
    phone_number = models.CharField(
        max_length=9,
        null=True,
    )
    country = models.CharField(
        max_length=100,
        null=True
    )
    town = models.CharField(
        max_length=100,
        null=True,
    )
    postal_code = models.CharField(
        max_length=5,
        null=True
    )
    street = models.CharField(
        max_length=100,
        null=True,
    )
    local_number = models.CharField(
        max_length=10,
        null=True,
        blank=True,
    )
    comments = models.TextField(
        blank = True,
    )
    created = models.DateTimeField(
        auto_now_add=True,
    )
    updated = models.DateTimeField(
        auto_now=True,
    )

    def __str__(self):
        return self.user.username