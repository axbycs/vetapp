from django.contrib import admin
from .models import Type, Visit

class TypeAdmin(admin.ModelAdmin):
    model = Type
    list_display = ('type_name', 'created', 'updated',)
    list_filter = ('created', 'updated',)

admin.site.register(Type, TypeAdmin)

class VisitAdmin(admin.ModelAdmin):
	model = Visit
	list_display = ('get_attendant', 'get_clinic', 'get_pet', 'visit_type', 'created', 'updated',)
	list_filter = ('created', 'updated',)
	
	def get_attendant(self, obj):
		return obj.appointment.attendant.user.username
	get_attendant.short_description = 'Visit attendant'

	def get_clinic(self, obj):
		return obj.appointment.clinic.email
	get_clinic.short_description = "Clinic"

	def get_pet(self, obj):
		return obj.appointment.pet.owner.user.username + " - " + obj.appointment.pet.pet_name
	get_pet.short_description = "Pet"

admin.site.register(Visit, VisitAdmin)