from django.db import models
from appointments.models import Appointment

class Type(models.Model):
    type_name = models.TextField(
    )
    aspects = models.TextField(
        blank=True,
    )
    created = models.DateTimeField(
        auto_now_add=True
    )
    updated = models.DateTimeField(
        auto_now=True
    )

    def __str__(self):
        return self.type_name

class Visit(models.Model):
    appointment = models.ForeignKey(
        Appointment,
        on_delete=models.CASCADE,
    )
    visit_type = models.ForeignKey(
        Type,
        on_delete=models.CASCADE,
    ) 
    resolution = models.TextField(
        blank = True,
    )
    comments = models.TextField(
        blank = True
    )
    created = models.DateTimeField(
        auto_now_add=True
    )
    updated = models.DateTimeField(
        auto_now=True
    )

    def __str__(self):
        return (self.appointment.attendant.user.username +
        " - " + self.appointment.clinic.email +
        " - " + self.appointment.pet.pet_name +
        " - " + self.appointment.pet.owner.user.username +
        " - " + self.visit_type.type_name)
