Aquest el projecte final de curs que tindrà modificacions respecte a la proposta aceptada:
1. Utilitzaré Django en compte de Ionic.
2. Mantindré el sistema gestor de productes de venta al public, el sistema gestor de cites i l'historial mèdic.
3. Si el temps ho permet faré també el sistema gestor de venta al public.
4. No faré el sistema de comunicació ni que es pugui sel·leccionar entre varies clíniques.
Observacions:
1. Els noms dels atributs i de les propies entitats seran traduïdes al anglès.
2. Les variables del codi seran escrites en anglès també.
